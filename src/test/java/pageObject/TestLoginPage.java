package pageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.pageObject.HomePage;
import pages.pageObject.LoginPage;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by IlyaMakarov on 12/21/2016.
 */
public class TestLoginPage {
    private WebDriver driver;
    private final static String LOGIN = "IlyaMakarov";
    private final static String PASSWORD = "Password1";

    @BeforeClass
    public void setUp() throws MalformedURLException {
        DesiredCapabilities capability = DesiredCapabilities.firefox();
        //capability.setCapability("version", "46");
        driver = new RemoteWebDriver(new URL("http://10.10.102.63:4444/wd/hub"), capability);
        //driver = new FirefoxDriver();
    }

    @AfterClass
    public void tearDown() {
        //driver.quit();
    }

    @Test
    public void loginTest() {
//        LoginPage loginPage = new LoginPage(driver);
//        HomePage homePage = loginPage.login(LOGIN, PASSWORD);
//        Assert.assertTrue(homePage.isSignOutButtonVisible(), "Sign out button is not displayed!");

        LoginPage loginPage = new LoginPage(driver);
        HomePage homePage = loginPage.login(LOGIN, PASSWORD);
        loginPage = homePage.signOut();
        //Assert.assertTrue(loginPage.isSubmitButtonDisplayed(), "Submit button is not displayed");

        loginPage = new LoginPage(driver);
        homePage = loginPage.login(LOGIN, PASSWORD);
        loginPage = homePage.signOut();

        loginPage = new LoginPage(driver);
        homePage = loginPage.login(LOGIN, PASSWORD);
        loginPage = homePage.signOut();

        loginPage = new LoginPage(driver);
        homePage = loginPage.login(LOGIN, PASSWORD);
        loginPage = homePage.signOut();

        loginPage = new LoginPage(driver);
        homePage = loginPage.login(LOGIN, PASSWORD);
        loginPage = homePage.signOut();

        loginPage = new LoginPage(driver);
        homePage = loginPage.login(LOGIN, PASSWORD);
        loginPage = homePage.signOut();

        loginPage = new LoginPage(driver);
        homePage = loginPage.login(LOGIN, PASSWORD);
        loginPage = homePage.signOut();

        Assert.assertTrue(loginPage.isSubmitButtonDisplayed(), "Submit button is not displayed");
    }
}
