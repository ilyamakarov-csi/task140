package pages.pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by IlyaMakarov on 12/21/2016.
 */
public class HomePage {
    private WebDriver driver;
    private final static By SIGNOUT_LINK =  By.cssSelector(".sign-out-span>a");

    public HomePage(WebDriver driver) {
        this.driver = driver;
    }

    public LoginPage signOut() {
        driver.findElement(SIGNOUT_LINK).click();
        return new LoginPage(driver);
    }

    public boolean isSignOutButtonVisible() {
        return driver.findElement(SIGNOUT_LINK).isDisplayed();
    }
}
